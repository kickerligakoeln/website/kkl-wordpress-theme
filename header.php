<!DOCTYPE html>
<html <?php language_attributes();

?>>
<head>
    <!-- .META -->
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title('|', true, 'right');

        // Add the blog name.
        bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) echo " | $site_description";

        // Add a page number if necessary:
        if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyten'), max($paged, $page));

        ?></title>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/public/css/styles.css"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php
    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
    ?>
</head>

<body <?php body_class('wp-editor content ' . get_post_field('post_name', get_post())); ?>>

<header>
    <div class="topbar">
        <section class="container">
            <nav>
                <ul>
                    <li class="mail"><a href="#"></a></li>
                    <li class="facebook"><a href="#"></a></li>
                    <li class="discord"><a href="#"></a></li>
                </ul>
            </nav>
        </section>
    </div>

    <div class="navbar">
        <section class="container">
            <div class="brand">
                <img
                        src="<?php bloginfo('template_url'); ?>/src/images/brand_weiss.svg"
                        alt="<?php bloginfo('title'); ?>" />
            </div>

            <?php get_sidebar(); ?>
        </section>
    </div>
</header>

