<nav>
    <?php wp_nav_menu(array(
        'theme_location' => 'primary',
        'menu_class' => 'navbar-main-content'
    )); ?>
</nav>