<!-- FOOTER  -->
<footer class="footer">

</footer>

<script src="<?php bloginfo('template_url'); ?>/public/js/main.js"></script>

<!-- .JS includes -->
<script type="text/javascript">
    var bloginfo = {
        url: '<?php bloginfo('url'); ?>'
    };
</script>

<?php
/* Always have wp_footer() just before the closing </body>
 * tag of your theme, or you will break many plugins, which
 * generally use this hook to reference JavaScript files.
 */

wp_footer();
?>
</body>
</html>
