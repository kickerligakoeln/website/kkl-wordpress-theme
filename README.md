`Stand 05/2022`

---

# Wordpress Theme: _Kickerliga_

Das offizielle Wordpress Theme zum beliebten Kickerliga Tool.

## Requirements

* Docker
* PHP 7.4
* Composer
* node version manager (nvm)

## Install Theme

```bash
> npm ci            # install node dependencies
> npm run build     # build javascript / scss
```

### development

```bash
> npm run dev       # run watcher
```

---

## Run Local Environment

To set up a local fresh running wordpress just start:

```bash
> docker-compose up
```

It does:

* if wordpress is not provided yet -> install Wordpress to a temp-folder and mount your theme
  in `.wordpress/wp-content/themes/kkl-wordpress-theme`
* serve a running webserver: ``http://localhost:8000``
* serve [Adminer](https://www.adminer.org/de/) at: ``http://localhost:8001``

> Tipp: find the database credentials in the `docker-compose.yml` ;-)