<?php get_header(); ?>

<main>
    <?php
    /* Run the loop to output the page.
     * If you want to overload this in a child theme then include a file
     * called loop-page.php and that will be used instead.
     */
    get_template_part( 'loop', 'index' );
    ?>
</main>

<?php get_footer(); ?>
