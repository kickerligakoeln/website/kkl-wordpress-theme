<?php

/**
 * Template Name: Home Page
 */

get_header();

?>

<main class="content">
    <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>
